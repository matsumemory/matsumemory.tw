### 檔案架構

.
├── component
| ├── images
| └── index
├── login
| ├── 會員登入 index.php  
| └── 會員註冊 register.php
| └── 忘記密碼 forgotPassword.php
| └── 設定密碼 setPassword.php
├── article
| ├── 文章列表 index.php  
| ├── 單一文章 content.php  
├── oldPhotos
| ├── 老照片列表 index.php  
| ├── 單一老照片 content.php  
├── search
| ├── 搜尋 index.php  
├── contribute
| ├── 我要投稿 index.php  
├── staticPages
| ├── 隱私權政策 privacy.php
└── 共用 frame.php
└── 共用 header.php
└── 共用 footer.php
└── 首頁 index.php
└── 靜態 404 staticMsg.php
