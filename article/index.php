<!DOCTYPE html>
<html lang="zh-Hant-TW">

<?
$title = "馬祖記憶庫";
$css = "../css/article.css";
include_once("../frame.php");
?>

<body>
  <div id="lsdv">
    <?php include("../header.php"); ?>

    <!-- 內容 -->
    <div class="container-fluid setEnd">
      <div class="container-fluid">
        <?
        $txt = "老照片";
        $img = "../images/article_oldPhoto.jpg";
        include_once("../component/articleTitle.php");
        ?>
      </div>
      <div class="container article">
        <div class="col-xs-12">
          <?php include("../component/breadcrumb.php"); ?>
        </div>
        <div class="col-xs-12 col-lg-3">
          <?php include("../component/categorymMenu.php"); ?>
        </div>
        <div class="col-xs-12 col-lg-9">
          <?php include("../component/categorymSubMenu.php"); ?>
          <?php include("../component/articleLists-x3.php"); ?>
          <?php include("../component/pagination.php"); ?>
        </div>
      </div>
    </div>
    <!-- //內容 -->

    <?php include("../footer.php"); ?>
  </div>
</body>

</html>