<!DOCTYPE html>
<html lang="zh-Hant-TW">

<?
$title = "馬祖記憶庫";
$css = "../css/article.css";
include_once("../frame.php");
?>

<body>
  <div id="lsdv">
    <?php include("../header.php"); ?>

    <!-- 內容 -->
    <div class="container-fluid setEnd">
      <div class="container article">
        <div class="col-xs-12 col-md-3-5">
          <img src="https://matsumemory.tw/sites/mazu/files/styles/medium/public/images/dsc02441.jpg?itok=lLwSwq4S" alt="1969年九二九復仇運動集合大會" class="article-img" />
        </div>
        <div class="col-xs-12 col-md-2-5">
          <h2 class="article-desc">桃園八德龍山寺</h2>
          <h3 class="article-title">撰寫者：原典創思規劃顧問有限公司</h3>
          <h3 class="article-title">分類：建築</h3>
          <h3 class="article-title">座標: N 121.2951190 E 24.9620090<a href="">地圖</a></h3>
          <buttom class="btn bn-out"><i class="icons icons-delete"></i>內容修改建議</buttom>
        </div>
        <div class="col-xs-12">
          桃園八德龍山寺位於桃園八德大湳公園內，為1970年代從馬祖南竿的牛峰境五靈公廟分靈而建。1970年代起，南竿牛角居民（現復興村）陸續遷居來臺，當時一位信徒陳源誠（顯俤）先生原是牛峰境五靈公廟的陳將軍附身，於1976年專程回馬祖分靈至桃園八德並成立廟宇，初期以家庭式經營形態處理廟宇，後因土地被徵收為公園用地，廟宇留存產生爭議，後在信眾協助下於2006年重組委員會，更名為桃園龍山寺。2017年，為了結構安全與歷久性，原有修建的家族都同意將廟宇文化資產捐給桃園市政府，由市政府進行「馬祖信仰中心整修工程」，原位重新打造恢復馬祖廟宇「封火山牆」的建築特色，並配合公園整修工程，廟埕廣場和公園緊密結合，2018年10月完工、2019年1月舉行安座典禮
          龍山寺主祀神為五靈公，陪祀神白馬尊王、陳將軍、半天陳夫人、臨水夫人、福德正神等神祇。宮廟祭儀活動於每年農曆正月十五日舉行元宵擺暝，於春夏之際依神明指示舉行補庫儀式，並透過扛乩方式提供信徒固定問事，2020年桃園八德龍山寺的粉絲專頁公告，服務時間為每周六晚上八點舉行。
        </div>
      </div>
      <div class="container">
        <h2>讀者共筆回饋</h2>
        <div class="col-xs-12">
          <div class="">
            <div class="">
              <i class="icons icons-delete"></i>王曉明 <time>2022-07-01</time>
            </div>
            <a href="" class="">回覆</a>
          </div>
        </div>
        <form action="" class="col-xs-12">
          <div class="col-xs-12 form-group">
            <textarea id="describe" cols="10" class="form-control" placeholder="請輸入"></textarea>
          </div>
          <buttom class="btn bn-out"><i class="icons icons-delete"></i>我要回饋</buttom>
        </form>
      </div>
    </div>
    <!-- //內容 -->

    <?php include("../footer.php"); ?>
  </div>
</body>

</html>