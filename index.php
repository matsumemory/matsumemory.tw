<!DOCTYPE html>
<html lang="zh-Hant-TW">

<?
$title = "馬祖記憶庫";
$css = "/css/index.css";
include_once("frame.php");
?>

<body>
  <div id="lsdv">
    <?php include("header.php"); ?>

    <!-- 內容 -->
    <?php include("component/index/video.php"); ?>
    <?php include("component/index/taxonomy.php"); ?>
    <?php include("component/index/photo.php"); ?>
    <?php include("component/index/contribute.php"); ?>
    <?php include("component/index/map.php"); ?>
    <?php include("component/index/article.php"); ?>
    <?php include("component/index/referenceGroup.php"); ?>
    <!-- //內容 -->

    <?php include("footer.php"); ?>
  </div>
</body>

</html>