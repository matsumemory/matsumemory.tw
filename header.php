<header class="header" id="js-hdFixed">
  <!-- DarkenBackground -->
  <div id="js-overlay" class="overlay" onclick="onCloseOverlay()"></div>

  <div class="container-fluid">
    <div class="container">
      <div class="col-xs-8 col-sm-9 col-md-10 col-lg-2 logo">
        <a href="/index.php" title="馬祖記憶庫">
          <?php include("images/logo.svg"); ?>
          <!-- <img src="/images/logo.svg" alt="馬祖記憶庫" class="logo-img" /> -->
        </a>
      </div>
      <div class="col-xs-4 col-sm-3 col-md-2 col-lg-10">
        <div class="col-xs-6 hidden-lg search-m" onclick="onOpenSearchBar()">
          <?php include("component/images/icons-search.svg"); ?>
        </div>
        <div class="col-xs-6 col-lg-3-5 mainNAV">
          <?php include("component/mainNAV.php"); ?>
        </div>
        <div class="col-xs-2-5 header-group-right hidden-xs hidden-sm hidden-md">
          <div class="col-xs-6">
            <?php include("component/search.php"); ?>
          </div>
          <div class="col-xs-3"><button class="btn bn-out btn-radius" onclick="self.location.href='/contribute/index.php'">我要投稿</button>
          </div>
          <div class="col-xs-3"><button class="btn bn-out btn-radius" onclick="self.location.href='/login/index.php'">登入註冊</button>
          </div>
        </div>
      </div>
      <div class="col-xs-12 hidden-lg" id="js-searchBar"><?php include("component/search.php"); ?></div>
    </div>
  </div>
</header>