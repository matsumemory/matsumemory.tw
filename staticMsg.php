<!DOCTYPE html>
<html lang="zh-Hant-TW">

<?
$title = "404";
$css = "/css/staticMsg.css";
include_once("frame.php");
?>

<body>
  <div id="lsdv">
    <?php include("header.php"); ?>

    <!-- 內容 -->
    <div class="container setEnd">
      <div class="staticMsg text-center">
        <img src="/images/error-404.svg" alt="" class="img-responsive staticMsg-img">
        <h2 class="staticMsg-title">404 ERROR <br />
          哦喔～這個頁面失蹤了......</h2>
        <div class="staticMsg-btn">
          <button class="btn bn-keep btn-md btn-radius" onclick="self.location.href='index.php'">回首頁</button> 或
          <button class="btn bn-keep btn-md btn-radius" onclick="self.location.href='index.php'">回上一頁</button>
        </div>
      </div>
    </div>
    <!-- //內容 -->

    <?php include("footer.php"); ?>
  </div>
</body>

</html>