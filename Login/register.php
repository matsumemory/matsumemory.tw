<!DOCTYPE html>
<html lang="zh-Hant-TW">

<?
$title = "馬祖記憶庫";
$css = "../css/login.css";
include_once("../frame.php");
?>

<body>
  <div id="lsdv">
    <?php include("../header.php"); ?>

    <!-- 內容 -->
    <div class="container-fluid setEnd">
      <div class="login">
        <h2 class="login-title">註冊新帳號</h2>
        <div class="row login-inner">
          <form action="" class="col-xs-12">
            <div class="col-xs-12 form-group has-error">
              <label for="email">電子郵件 *</label>
              <input type="email" name="email" class="form-control" placeholder="請輸入電子郵件" />
              <div class="login-tips">
                <img src="/images/icons-tips.svg" alt="填寫說明" class="login-tipsTitle" />
                <div class="login-tipsDesc"> 一個有效的電子郵件地址。所有由系統發出的電子郵件將寄往此地址。這電子郵件地址不會被公開，並且只在您想要取得新密碼、接收消息或通知時，我們才會用此電子郵件地址寄信給您。
                </div>
              </div>
              <div class="errMsg">格式錯誤</div>
            </div>
            <div class="col-xs-12 form-group">
              <label for="petName">真實姓名 *</label>
              <input type="text" name="petName" class="form-control" placeholder="請輸入暱稱" />
            </div>
            <div class="col-xs-12 form-group">
              <label for="petName">暱稱 *</label>
              <input type="text" name="petName" class="form-control" placeholder="請輸入真實姓名" />
            </div>
            <div class="col-xs-12 form-group">
              <label for="petName">聯絡電話 *</label>
              <input type="text" name="petName" class="form-control" placeholder="請輸入聯絡電話" />
            </div>
            <div class="col-xs-12 form-group">
              <label for="petName">聯絡地址</label>
              <input type="text" name="petName" class="form-control" placeholder="請輸入聯絡地址" />
            </div>
            <div class="col-xs-8 col-sm-9  form-group">
              <label for="captcha">驗證碼</label>
              <input type="text" name="captcha" class="form-control" placeholder="請輸入驗證碼" />
            </div>
            <div class="col-xs-4 col-sm-3">
              <img src="../images/login_captcha.png" alt="驗證碼" class="login-captchaImg" />
            </div>
            <div class="col-xs-12">
              <button class="btn btn-md btn-block bn-keep">登入</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- //內容 -->

    <?php include("../footer.php"); ?>
  </div>
</body>

</html>