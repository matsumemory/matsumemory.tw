<!DOCTYPE html>
<html lang="zh-Hant-TW">

<?
$title = "馬祖記憶庫";
$css = "../css/login.css";
include_once("../frame.php");
?>

<body>
  <div id="lsdv">
    <?php include("../header.php"); ?>

    <!-- 內容 -->
    <div class="container-fluid setEnd">
      <div class="login">
        <h2 class="login-title">設定密碼 </h2>
        <p class="login-desc">經由電子郵件傳送新密碼</p>
        <div class="row login-inner">
          <form action="" class="col-xs-12">
            <div class="col-xs-12 form-group has-error">
              <label for="password">電子郵件</label>
              <input type="password" name="password" class="form-control" placeholder="請輸入電子郵件" />
              <div class="errMsg">長度不能少於6個字元，加入大、小寫字母、數字及標點符號</div>
            </div>
            <div class="col-xs-8 col-sm-9  form-group">
              <label for="captcha">驗證碼</label>
              <input type="text" name="captcha" class="form-control" placeholder="請輸入驗證碼" />
            </div>
            <div class="col-xs-4 col-sm-3">
              <img src="../images/login_captcha.png" alt="驗證碼" class="login-captchaImg" />
            </div>
            <div class="col-xs-12">
              <button class="btn btn-md btn-block bn-keep">送出</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- //內容 -->

    <?php include("../footer.php"); ?>
  </div>
</body>

</html>