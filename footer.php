<footer class="footer">
  <div class="container">
    <div class="row footerList">
      <div class="col-xs-12 col-sm-8">
        © Matsumemory 2022<br />
        本網站由連江縣政府文化處負責維護管理，部分圖片之著作權利狀態仍未經確認，僅限本平台瀏覽。
      </div>
      <div class="col-xs-12 col-sm-4 text-right">
        <button class="btn bn-out btn-radius">控制中心</button>
      </div>
    </div>
  </div>
</footer>