<!DOCTYPE html>
<html lang="zh-Hant-TW">

<?
$title = "馬祖記憶庫";
$css = "../css/contribute.css";
include_once("../frame.php");
?>

<body>
  <div id="lsdv">
    <?php include("../header.php"); ?>

    <!-- 內容 -->
    <div class="container-fluid setEnd">
      <div class="container contribute">
        <div class="col-xs-12">
          <?php include("../component/breadcrumb.php"); ?>
        </div>
        <p class="contribute-introduction">馬祖四鄉五島匯聚了各方討生活的人們，對早期往返漁場的漁人和商販、撤退的士兵而言，38年後的馬祖成了第二家鄉；而對島嶼上出生的娃兒來說，對岸是原鄉，家鄉成了腳下所踏的馬祖。再後來，馬祖則成了新一代遷臺鄉親遙遠的記憶。<br />
          這是大時代下馬祖人的歷史，馬祖記憶庫希望一起記得您的故事。馬祖記憶庫平台歡迎鄉親以可公開使用的影像為主題，和我們分享對您家族、個人具有特殊意義的故事。投稿後，將有專人聯繫您，確定影像與文章內容無誤後，將刊登在馬祖記憶庫平台上。<br />
          您的故事將有助於馬祖記憶庫累積馬祖人的集體歷史，讓下一代更認識馬祖這塊土地。</p>
        <form action="" class="col-xs-12">
          <!-- 照片 -->
          <div class="row contribute-floor">
            <h2 class="contribute-title">照片 <i class="icons icons-arrow top"></i></h2>
            <div class="contribute-content">

              <div class="col-xs-12 form-group">
                <fieldset>
                  <legend>照片上傳 *</legend>
                  <div class="col-xs-12">
                    <div class="contribute-uploadPhoto">
                      <div class="contribute-uploadPhoto-left">
                        <img src="https://matsumemory.tw/sites/mazu/files/styles/large/public/images/9-154.jpg" alt="" class="contribute-uploadPhoto-img" />
                        <div>
                          9-154.jpg
                          <p class="contribute-uploadPhoto-fileSize">431.31 KB</p>
                        </div>
                      </div>
                      <div class="contribute-uploadPhoto-right">
                        <buttom class="btn bn-link" title="刪除"><i class="icons icons-delete"></i></buttom>
                      </div>
                    </div>

                    <div class="contribute-uploadPhoto">
                      <div class="contribute-uploadPhoto-left">
                        <img src="https://matsumemory.tw/sites/mazu/files/styles/medium/public/images/ma_zu_chuan_tong_biao_yan_yi_shu_gu_ban_le_.jpg?itok=Xf5rmKvK" alt="" class="contribute-uploadPhoto-img" />
                        <div>
                          9-154.jpg
                          <p class="contribute-uploadPhoto-fileSize">431.31 KB</p>
                        </div>
                      </div>
                      <div class="contribute-uploadPhoto-right">
                        <buttom class="btn bn-link" title="刪除"><i class="icons icons-delete"></i></buttom>
                      </div>
                    </div>

                  </div>
                  <div class="col-xs-12">
                    <buttom class="btn bn-out" title="照片上傳"><i class="icons icons-upload"></i>照片上傳</buttom>
                    <p class="form-extra">檔案必須小於 8 MB，
                      圖像必須大於 768x768 像素，副檔名：jpg jpeg。</p>
                  </div>
                </fieldset>
              </div>

            </div>
          </div>
          <!-- 照片 end -->
          <!-- 投稿資料 -->
          <div class="row contribute-floor">
            <h2 class="contribute-title">投稿資料 <i class="icons icons-arrow top"></i></h2>
            <div class="contribute-content">
              <div class="col-xs-12 col-md-6 form-group has-error">
                <label for="name1">名稱 *</label>
                <input type="text" id="name1" class="form-control" placeholder="請輸入" />
                <div class="errMsg">格式錯誤</div>
              </div>
              <div class="col-xs-12 col-md-6 form-group">
                <label for="name2">拍攝者 *</label>
                <input type="text" id="name2" class="form-control" placeholder="請輸入" />
              </div>

              <div class="col-xs-12 col-md-6 form-group clearfix">
                <label for="name3">撰寫者</label>
                <input type="text" id="name3" class="form-control" placeholder="請輸入" />
              </div>
              <div class="col-xs-12 col-md-6 form-group">
                <label for="name4">投稿者</label>
                <input type="text" id="name4" class="form-control" placeholder="請輸入" />
              </div>

              <div class="col-xs-12 col-md-6 form-group">
                <label for="name5">取得方式</label>
                <input type="text" id="name5" class="form-control" placeholder="請輸入" />
              </div>
              <div class="col-xs-12 col-md-6 form-group">
                <label for="name6">取得對象</label>
                <input type="text" id="name6" class="form-control" placeholder="請輸入" />
              </div>

              <div class="col-xs-12 form-group has-error">
                <fieldset>
                  <legend>照片分類 *</legend>
                  <div class="row form-control">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">中央社58年前老照片</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo2" name="clasphoto" value="2"> <label for="photo2">中正國小老照片</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">個人收藏</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo2" name="clasphoto" value="2"> <label for="photo2">典藏馬祖</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">文物館50年代老照片</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">文獻中心 - 1950年代</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">文獻中心 - 1960年代</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo2" name="clasphoto" value="2"> <label for="photo2">文獻中心 - 1970年代</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">文獻中心 - 1980年代</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo2" name="clasphoto" value="2"> <label for="photo2">文獻中心 - 東引老照片</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">文獻中心 - 馬祖老照片</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">時空寄情</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">莒光老照片市集</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                      <input type="checkbox" id="photo1" name="clasphoto" value="1"> <label for="photo1">馬祖高中老照片</label>
                    </div>
                  </div>
                  <div class="errMsg">此欄位必填</div>
                </fieldset>
              </div>

              <div class="col-xs-12 col-md-6 form-group clearfix">
                <label for="classification1">分類1</label>
                <input type="text" id="classification1" class="form-control" placeholder="請輸入" />
              </div>
              <div class="col-xs-12 col-md-6 form-group">
                <label for="classification2">分類2</label>
                <input type="text" id="classification2" class="form-control" placeholder="請輸入" />
              </div>

              <div class="col-xs-12 form-group">
                <label for="describe">描述 *</label>
                <textarea id="describe" cols="10" class="form-control" placeholder="請輸入"></textarea>
              </div>
            </div>
          </div>
          <!-- 投稿資料 end -->
          <!-- 時間 -->
          <div class="row contribute-floor">
            <h2 class="contribute-title">時間 <i class="icons icons-arrow top"></i></h2>
            <div class="contribute-content">
              <div class="col-xs-12 col-md-6 form-group">
                <fieldset>
                  <legend>拍攝時間</legend>
                  <div class="form-fieldset">

                    <select id="delcityid" name="delcityid" class="form-control col-4">
                      <option value="" style="display: none">年</option>
                      <option value="2022">
                        2022
                      </option>
                      <option value="2023">
                        2023
                      </option>
                    </select>

                    <select id="delcityid" name="delcityid" class="form-control col-4">
                      <option value="" style="display: none">月</option>
                      <option value="1月">
                        1月
                      </option>
                      <option value="2月">
                        2月
                      </option>
                    </select>

                    <select id="delcityid" name="delcityid" class="form-control col-4">
                      <option value="" style="display: none">日</option>
                      <option value="1">
                        1
                      </option>
                      <option value="2">
                        2
                      </option>
                    </select>

                  </div>
                </fieldset>
              </div>
              <div class="col-xs-12 col-md-6 form-group">
                <label for="time">時間涵蓋</label>
                <input type="text" id="time" class="form-control" placeholder="請輸入" />

              </div>
            </div>
          </div>
          <!-- 時間 end -->
          <!-- 地點 -->
          <div class="row contribute-floor">
            <h2 class="contribute-title">地點 <i class="icons icons-arrow top"></i></h2>
            <div class="contribute-content">
              <div class="col-xs-12 col-md-6 form-group">
                <fieldset>
                  <legend>地點</legend>
                  <div class="form-fieldset">

                    <div class="form-addonGroup col-6">
                      <div class="form-addon">經度</div>
                      <input type="text" id="time" class="form-control" placeholder="請輸入" />
                    </div>
                    <div class="form-addonGroup col-6">
                      <div class="form-addon">緯度</div>
                      <input type="text" id="time" class="form-control" placeholder="請輸入" />
                    </div>

                  </div>
                </fieldset>
              </div>

            </div>
          </div>
          <!-- 地點 end -->
          <div class="col-xs-12 contribute-btn">
            <button class="btn btn-md bn-out">取消</button>
            <button class="btn btn-md bn-keep">儲存</button>
          </div>
        </form>
      </div>
    </div>
    <!-- //內容 -->

    <?php include("../footer.php"); ?>
  </div>
</body>

</html>