<div class="col-xs-12 pagination">
  <div class="pagination-group">
    <button type="button" class="btn bn-out">
      <?php include("images/icons-arrow.svg"); ?> </button>
    <button type="button" class="btn bn-keep">1</button>
    <button type="button" class="btn bn-out">2</button>
    ...
    <button type="button" class="btn bn-out">40</button>
    <button type="button" class="btn bn-out pagination-right">
      <?php include("images/icons-arrow.svg"); ?> </button>
  </div>
</div>