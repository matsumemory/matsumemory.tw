<div class="container-fluid part referenceGroup-extra">
  <div class="container referenceGroup">
    <div class="referenceGroup-inner">
      <div class="referenceGroup-item">
        <a href="http://memory.culture.tw/" target="_blank" title="國家文化記憶庫">
          <img src="/images/memoryCultureLogo.svg" alt="國家文化記憶庫" class="referenceGroup-img" />
        </a>
        <h2 class="text-hide">國家文化記憶庫</h2>
        <h3 class="text-hide">國家文化記憶庫是一個走入日常生活，尋找臺灣故事及原生文化的行動，讓我們一起挖掘地方，收集充滿溫度的故事，展示多采多姿的臺灣印象。</h3>
      </div>
      <div class="referenceGroup-item">
        <a href="https://www.matsugod.net/" target="_blank" title="馬祖好神">
          <img src="/images/matsugodLogo_s.jpg" alt="馬祖好神" class="referenceGroup-img" />
        </a>
        <h2 class="text-hide">馬祖好神</h2>
        <h3 class="text-hide">
          昔日討海維生的馬祖人習於向神明尋求慰藉，人與神具有緊密關係，寺廟密度極高，對於傳統習俗、信仰與節慶文化的保存與傳承也不遺餘力，因此以馬祖的民間信仰文化為主題，介紹閩東信仰及主要信仰神祇。</h3>
      </div>
      <div class="referenceGroup-item">
        <a href="https://www.matsufood.tw/" target="_blank" title="馬祖好食">
          <img src="/images/matsufoodLogo.png" alt="馬祖好食" class="referenceGroup-img" />
        </a>
        <h2 class="text-hide">馬祖好食</h2>
        <h3 class="text-hide">馬祖的飲食文化與先民的移入有很深的關聯，馬祖先民多來自福建的連江與長樂，地域上慣稱閩東，因為臨海、多以捕魚為生，因此食材以海鮮為主，飲食習慣與風俗多傳承自原鄉福州。</h3>
      </div>
    </div>
  </div>
</div>