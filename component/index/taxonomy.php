<div class="container part taxonomy">
  <h2 class="text-hide">記憶馬祖</h2>
  <div class="taxonomy-inner">
    <a href="" target="_blank" title="民俗信仰" class="taxonomy-item">
      <img src="images/taxonomy_folkways.jpg" alt="民俗信仰" class="taxonomy-img" />
      <div class="taxonomy-textGroup">
        <h3>民俗信仰</h3>
        <small>300 items</small>
      </div>
    </a>
    <a href="" target="_blank" title="藝術人文" class="taxonomy-item">
      <img src="images/taxonomy_humanities.jpg" alt="藝術人文" class="taxonomy-img" />
      <div class="taxonomy-textGroup">
        <h3>藝術人文</h3>
        <small>300 items</small>
      </div>
    </a>
    <a href="" target="_blank" title="戰地馬祖" class="taxonomy-item">
      <img src="images/taxonomy_battlefield.jpg" alt="戰地馬祖" class="taxonomy-img" />
      <div class="taxonomy-textGroup">
        <h3>戰地馬祖</h3>
        <small>300 items</small>
      </div>
    </a>
    <a href="" target="_blank" title="村落記事" class="taxonomy-item">
      <img src="images/taxonomy_village.jpg" alt="村落記事" class="taxonomy-img" />
      <div class="taxonomy-textGroup">
        <h3>村落記事</h3>
        <small>300 items</small>
      </div>
    </a>
  </div>
</div>