<div class="container-fluid">
  <video autoplay muted class="video">
    <source src="video/index.mp4" type="video/mp4">
  </video>

  <div class="video-content">
    <div class="container video-inner">
      <h2 class="video-content-title">馬祖 / <br />文化記憶庫</h2>
      <h3 class="video-content-promote"> 我們蒐集與分享馬祖各種記憶與在地知識 </h3>
      <h3 class="video-content-promote">期待有更多人能一起參與！</h3>
      <button class="btn btn-md bn-keep btn-radius video-content-btn" onclick="self.location.href='/contribute/index.php'">
        <img src="/images/icons-photo.svg" />
        我要投稿
      </button>
    </div>
  </div>
</div>