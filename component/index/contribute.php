<div class="container-fluid part contribute">
  <div class="container">
    <div class="col-xs-12 col-sm-4 col-lg-5 col-sm-push-8 col-lg-push-7">
      <h2 class="text-hide">我要投稿</h2>
      <h3>馬祖記憶庫平台歡迎鄉親以可公開使用的影像為主題，和我們分享對您家族、個人具有特殊意義的故事。投稿後，將有專人聯繫您，確定影像與文章內容無誤後，將刊登在馬祖記憶庫平台上。
        <br />您的故事將有助於馬祖記憶庫累積馬祖人的集體歷史，讓下一代更認識馬祖這塊土地。
      </h3>
      <button class="btn btn-md bn-keep btn-radius video-content-btn" onclick="self.location.href='/contribute/index.php'">
        <img src="/images/icons-photo.svg" />
        我要投稿
      </button>
    </div>
  </div>
</div>