<!-- 大小網差異點
ui   -->
<div class="categorymMenu">
  <ul class="categorymMenu-lists">
    <li>
      <h1 class="categorymMenu-title">老照片</h1>
    </li>
    <li><a href="" class="categorymMenu-item" title="中央社58年前老照片">中央社58年前老照片</a></li>
    <li><a href="" class="categorymMenu-item active" title="中正國小老照片">中正國小老照片</a></li>
    <li><a href="" class="categorymMenu-item" title="個人收藏">個人收藏</a></li>
    <li><a href="" class="categorymMenu-item" title="時空寄情">典藏馬祖</a></li>
    <li><a href="" class="categorymMenu-item" title="文物館50年代老照片">文物館50年代老照片</a></li>
    <li><a href="" class="categorymMenu-item" title="文獻中心">文獻中心</a></li>
    <li><a href="" class="categorymMenu-item" title="典藏馬祖">時空寄情</a></li>
    <li><a href="" class="categorymMenu-item" title="莒光老照片市集">莒光老照片市集</a></li>
    <li><a href="" class="categorymMenu-item" title="馬祖高中老照片">馬祖高中老照片</a></li>
  </ul>
</div>

<div class="categorymMenu-m">
  <h1 class="categorymMenu-selectItem">中正國小老照片</h1>
  <ul class="categorymMenu-lists">
    <li><a href="" class="categorymMenu-item" title="中央社58年前老照片">中央社58年前老照片</a></li>
    <li><a href="" class="categorymMenu-item active" title="中正國小老照片">中正國小老照片</a></li>
    <li><a href="" class="categorymMenu-item" title="個人收藏">個人收藏</a></li>
    <li><a href="" class="categorymMenu-item" title="時空寄情">典藏馬祖</a></li>
    <li><a href="" class="categorymMenu-item" title="文物館50年代老照片">文物館50年代老照片</a></li>
    <li><a href="" class="categorymMenu-item" title="文獻中心">文獻中心</a></li>
    <li><a href="" class="categorymMenu-item" title="典藏馬祖">時空寄情</a></li>
    <li><a href="" class="categorymMenu-item" title="莒光老照片市集">莒光老照片市集</a></li>
    <li><a href="" class="categorymMenu-item" title="馬祖高中老照片">馬祖高中老照片</a></li>
  </ul>
</div>