<ul class="row sideNav" id="js-sideNav">
  <li class="hidden-lg sideNav-list">
    <a href="/index.php" title="馬祖記憶庫">馬祖記憶庫</a>
  </li>
  <li class="hidden-lg sideNav-list">
    <a href="/index.php" title="回首頁">回首頁</a>
  </li>
  <li class="hidden-lg sideNav-list">
    <a href="/login/index.php" title="登入/註冊">登入/註冊</a>
  </li>
  <li class="hidden-lg sideNav-list">
    <a href="/contribute/index.php" title="我要投稿">我要投稿</a>
  </li>
  <li class="js-sideNavLists sideNav-lists">
    <img src="../component/images/icons-arrow.svg" class="sideNav-backArrow" />老照片
    <div class="listItem">
      <ul>
        <li class="listItem-item"><a href="" title="中央社58年前老照片">中央社58年前老照片</a></li>
        <li class="listItem-item"><a href="" title="中正國小老照片">中正國小老照片</a></li>
        <li class="listItem-item"><a href="" title="個人收藏">個人收藏</a></li>
        <li class="listItem-item"><a href="" title="時空寄情">典藏馬祖</a></li>
        <li class="listItem-item"><a href="" title="文物館50年代老照片">文物館50年代老照片</a></li>
        <li class="listItem-item"><a href="" title="文獻中心">文獻中心</a></li>
        <li class="listItem-item"><a href="" title="典藏馬祖">時空寄情</a></li>
        <li class="listItem-item"><a href="" title="莒光老照片市集">莒光老照片市集</a></li>
        <li class="listItem-item"><a href="" title="馬祖高中老照片">馬祖高中老照片</a></li>
      </ul>
    </div>
  </li>
  <li class="js-sideNavLists sideNav-lists">
    <img src="../component/images/icons-arrow.svg" class="sideNav-backArrow" />民俗信仰
    <div class="listItem">
      <ul>
        <li class="listItem-item"><a href="" title="建築">建築</a></li>
        <li class="listItem-item"><a href="" title="文物">文物</a></li>
        <li class="listItem-item"><a href="" title="祭儀活動">祭儀活動</a></li>
        <li class="listItem-item"><a href="" title="神明故事">神明故事</a></li>
        <li class="listItem-item"><a href="" title="生命禮俗">生命禮俗</a></li>
      </ul>
    </div>
  </li>
  <li class="js-sideNavLists sideNav-lists">
    <img src="../component/images/icons-arrow.svg" class="sideNav-backArrow" />藝術人文
    <div class="listItem">
      <ul>
        <li class="listItem-item"><a href="" title="鼓板">鼓板</a></li>
        <li class="listItem-item"><a href="" title="傳統建築">傳統建築</a></li>
        <li class="listItem-item"><a href="" title="剪花">剪花</a></li>
        <li class="listItem-item"><a href="" title="方言">方言</a></li>
        <li class="listItem-item"><a href="" title="歌謠">歌謠</a></li>
        <li class="listItem-item"><a href="" title="飲食">飲食</a></li>
      </ul>
    </div>
  </li>
  <li class="js-sideNavLists sideNav-lists">
    <img src="../component/images/icons-arrow.svg" class="sideNav-backArrow" />戰地馬祖
    <div class="listItem">
      <ul>
        <li class="listItem-item"><a href="" title="戰地景觀">戰地景觀</a></li>
        <li class="listItem-item"><a href="" title="戰地記事">戰地記事</a></li>
        <li class="listItem-item"><a href="" title="戰地建設">戰地建設</a></li>
        <li class="listItem-item"><a href="" title="戰地生活">戰地生活</a></li>
        <li class="listItem-item"><a href="" title="戰略組織">戰略組織</a></li>
        <li class="listItem-item"><a href="" title="教育娛樂">教育娛樂</a></li>
        <li class="listItem-item"><a href="" title="戰地東引">戰地東引</a></li>
      </ul>
    </div>
  </li>
  <li class="js-sideNavLists sideNav-lists">
    <img src="../component/images/icons-arrow.svg" class="sideNav-backArrow" />村落記事
    <div class="listItem">
      <ul>
        <li class="listItem-item"><a href="" title="南竿">南竿</a></li>
        <li class="listItem-item"><a href="" title="北竿">北竿</a></li>
        <li class="listItem-item"><a href="" title="莒光">莒光</a></li>
        <li class="listItem-item"><a href="" title="東引">東引</a></li>
      </ul>
    </div>
  </li>
</ul>

<button class="burgerBtn" aria-hidden="true" onclick="onOpenSideNav()">
  <?php include("images/icons-menu.svg"); ?>
</button>